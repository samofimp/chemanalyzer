create table if not exists compound
(id bigint primary key
auto_increment not null,
smilesname clob not null,
inchiname clob,
molfile clob not null,
structure blob not null,
fingerprint blob not null);
create table if not exists compounds_similarity
(id bigint primary key
auto_increment not null,
compound1_id bigint not null,
compound2_id bigint not null,
coeff double not null,
foreign key(compound1_id)
references compound(id)
on update cascade,
foreign key(compound2_id)
references compound(id)
on update cascade);
create table if not exists patent
(id bigint primary key
auto_increment not null,
publication_name varchar(1000) not null,
publication_number varchar(100) not null,
publication_date date not null,
applicant varchar(2000) not null,
ipc_classification varchar(5000) not null);
create table if not exists compound_in_patent
(id bigint primary key
auto_increment not null,
compound_id bigint not null,
patent_id bigint not null,
foreign key(compound_id)
references compound(id)
on update cascade,
foreign key(patent_id)
references patent(id)
on update cascade);
create table if not exists patents_similarity
(id bigint primary key
auto_increment not null,
patent1_id bigint not null,
patent2_id bigint not null,
coeff double not null,
foreign key(patent1_id)
references patent(id)
on update cascade,
foreign key(patent2_id)
references patent(id)
on update cascade);