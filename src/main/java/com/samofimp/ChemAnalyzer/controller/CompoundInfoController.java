package com.samofimp.ChemAnalyzer.controller;

import com.samofimp.ChemAnalyzer.model.Compound;
import com.samofimp.ChemAnalyzer.model.CompoundDB;
import com.samofimp.ChemAnalyzer.model.CompoundsRepository;
import com.samofimp.ChemAnalyzer.model.PatentDB;
import org.apache.commons.io.IOUtils;
import org.openscience.cdk.exception.CDKException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

/** Контроллер информации о химическом соединении */
@Controller
public class CompoundInfoController {
    /** Репозиторий химических соединений */
    @Autowired
    CompoundsRepository compoundsRepository;

    /** Показ страницы с информацией о выбранном химическом соединении
     * @param compoundId идентификатор химического соединения
     * @return представление страницы с информацией о выбранном химическом соединении
     * */
    @RequestMapping(value = "/compounds/{compoundId}", method = RequestMethod.GET)
    public String showCompoundInfo(@PathVariable String compoundId, Model model) throws SQLException, IOException, CDKException {
        Long compoundIdLong = Long.parseLong(compoundId);
        Long fromPage = compoundIdLong / 10;
        CompoundDB currentCompound = compoundsRepository.findById(compoundIdLong);
        Compound compound = currentCompound.FromDatabaseTypes();
        PatentDB fromPatent = compoundsRepository.getPatentWithCompound(compoundIdLong);
        model.addAttribute("fromPage", fromPage);
        model.addAttribute("currentCompound", compound);
        model.addAttribute("fromPatent", fromPatent);
        return "compound-info";
    }

    /** Загрузка изображения со структурным представлением химического соединения
     * @param compoundId идентификатор химического соединения
     * @return изображение со структурным представлением химического соединения
     * */
    @RequestMapping(value = "/compound-image/{compoundId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImageAsResponseEntity(@PathVariable String compoundId) throws SQLException, IOException {
        HttpHeaders headers = new HttpHeaders();
        Long compoundIdLong = Long.parseLong(compoundId);
        CompoundDB compound = compoundsRepository.findById(compoundIdLong);
        InputStream inputStream = compound.getStructureRepresentation().getBinaryStream();
        byte[] image = IOUtils.toByteArray(inputStream);
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(image, headers, HttpStatus.OK);
        return responseEntity;
    }
}
