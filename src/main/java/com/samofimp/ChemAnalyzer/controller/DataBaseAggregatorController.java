package com.samofimp.ChemAnalyzer.controller;

import com.samofimp.ChemAnalyzer.model.ArchivesExtractor;

import com.samofimp.ChemAnalyzer.model.DatabaseAggregator;
import org.openscience.cdk.exception.CDKException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/** Контроллер сборщика информации о химических соединениях и патентах */
@Controller
public class DataBaseAggregatorController {

    /** Список архивов в директории с архивами */
    ArrayList<String> archives;
    /** Директория с архивами */
    String archivesDirectory;
    /** Извлекатель патентов из архивов */
    ArchivesExtractor extractor;
    /** Сборщик информации о химических соединениях и патентах */
    @Autowired
    DatabaseAggregator aggregator;


    /** Поток обработки соединений и добавления их в базу данных */
    Thread aggregationThread = new Thread(){
        public void run(){
            try {
                //aggregator = new DatabaseAggregator(archivesDirectory + "/unpacked");
                aggregator.launch(archivesDirectory + "/unpacked");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (CDKException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
        }
    };

    /** Поток извлечения патентов из архивов */
    Thread extractionThread = new Thread(){
        public void run(){
            try {
                extractor = new ArchivesExtractor(archivesDirectory, archives);
                extractor.launch();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    /** Получение страницы сборщика данных
     * @return представление страницы сборщика данных
     * */
    @GetMapping("/aggregator")
    public String aggregator(Model model) {
        // Получаем список архивов из папки archives
        archivesDirectory = System.getProperty("user.dir") + "/archives";
        model.addAttribute("path", archivesDirectory);
        File folder = new File(archivesDirectory);
        archives = new ArrayList<String>(Arrays.asList(folder.list()));
        for (int i = 0; i < archives.size(); i++) {
            String currentName = archives.get(i);
            int pointIndex = currentName.lastIndexOf(".");
            if (pointIndex < 0) {
                archives.remove(i);
                i--;
                continue;
            }
            String substring = currentName.substring(pointIndex);
            if (!substring.equals(".tar") && !substring.equals(".zip") && !substring.equals(".ZIP")) {
                archives.remove(i);
                i--;
            }
        }
        model.addAttribute("archives", archives);
        return "aggregator";
    }

    /** Запуск сбора данных */
    @RequestMapping(value = "/start-aggregating", method = RequestMethod.GET)
    public void startAggregating(HttpServletResponse response) throws InterruptedException {
        if (aggregationThread.getState() == Thread.State.NEW)
            aggregationThread.start();
        response.setStatus(HttpServletResponse.SC_OK);
    }

    /** Получение текущего прогресса сбора данных */
    @RequestMapping(value = "/current-progress-aggregate", method = RequestMethod.GET)
    public void updateProgress(HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("text/html");
        if (aggregationThread.getState() == Thread.State.NEW) {
            response.getWriter().write("0");
        }
        else {
            response.getWriter().write(String.valueOf(aggregator.getAggregationProgress()));
        }
        response.getWriter().flush();
    }

    /** Запуск извлечения патентов из архивов */
    @RequestMapping(value = "/start-extracting", method = RequestMethod.GET)
    public void extractArchives(HttpServletResponse response) throws IOException {
        extractionThread.start();
        response.setStatus(HttpServletResponse.SC_OK);
    }

    /** Получение текущего прогресса разархивирования */
    @RequestMapping(value = "/current-progress-extract", method = RequestMethod.GET)
    public void extractProgress(HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("text/html");
        if (extractionThread.getState() == Thread.State.NEW) {
            response.getWriter().write("0");
        }
        else {
            response.getWriter().write(String.valueOf(extractor.getExtractionProgress()));
        }
        response.getWriter().flush();
    }
}
