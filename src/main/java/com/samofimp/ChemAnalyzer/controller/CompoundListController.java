package com.samofimp.ChemAnalyzer.controller;

import com.samofimp.ChemAnalyzer.model.Compound;
import com.samofimp.ChemAnalyzer.model.CompoundDB;
import com.samofimp.ChemAnalyzer.model.CompoundsRepository;
import org.openscience.cdk.exception.CDKException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/** Контроллер списка химических соединений */
@Controller
public class CompoundListController {
    /** Репозиторий химических соединений */
    @Autowired
    CompoundsRepository compoundsRepository;

    /** Показ страницы со списком химических соединений
     * @param pageNumber номер страницы списка
     * @return представление страницы со списком химических соединений
     * */
    @RequestMapping(value = "/compound-list", method = RequestMethod.GET)
    public String showPatentListOnPage (@RequestParam(value = "page", required = false) String pageNumber, Model model) throws SQLException, IOException, CDKException {
        int compoundCount = compoundsRepository.findTotalCount();
        int availablePages = (compoundCount / 10 + 1);
        model.addAttribute("pages", String.valueOf(availablePages));
        if (pageNumber == null || pageNumber.equals("0"))
            pageNumber = "1";
        model.addAttribute("pageNum", pageNumber);
        Long firstId = new Long((Integer.parseInt(pageNumber) - 1) * 10 + 1);
        Long lastId = new Long(Integer.parseInt(pageNumber) * 10);
        List<CompoundDB> compoundsDB = compoundsRepository.findFromTo(firstId, lastId);
        List<Compound> compounds = new ArrayList<Compound>();
        for (CompoundDB compoundDB : compoundsDB) {
            if (compoundDB.getSMILESRepresentation().length() > 0) {
                compounds.add(compoundDB.FromDatabaseTypes());
            }
        }
        model.addAttribute("compounds", compounds);
        return "compound-list";
    }
}
