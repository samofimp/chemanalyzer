package com.samofimp.ChemAnalyzer.controller;

import com.samofimp.ChemAnalyzer.model.PatentDB;
import com.samofimp.ChemAnalyzer.model.PatentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class PatentListController {
    @Autowired
    PatentsRepository patentsRepository;

    @RequestMapping(value = "/patent-list", method = RequestMethod.GET)
    public String showPatentListOnPage (@RequestParam(value = "page", required = false) String pageNumber, Model model){
        int patentCount = patentsRepository.findTotalCount();
        int availablePages = (patentCount / 10 + 1);
        model.addAttribute("pages", String.valueOf(availablePages));
        if (pageNumber == null || pageNumber.equals("0"))
            pageNumber = "1";
        model.addAttribute("pageNum", pageNumber);
        Long firstId = new Long((Integer.parseInt(pageNumber) - 1) * 10 + 1);
        Long lastId = new Long(Integer.parseInt(pageNumber) * 10);
        List<PatentDB> patents = patentsRepository.findFromTo(firstId, lastId);
        model.addAttribute("patents", patents);
        return "patent-list";
    }
}
