package com.samofimp.ChemAnalyzer.controller;

import com.samofimp.ChemAnalyzer.model.PatentDB;
import com.samofimp.ChemAnalyzer.model.PatentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/** Контроллер схожих патентов */
@Controller
public class SimilarPatentsController {
    /** Репозиторий патентов */
    @Autowired
    PatentsRepository patentsRepository;

    /** Получение страницы со списком схожих с выбранным патентов
     * @param patentId идентификатор патента
     * @return представление страницы со списком схожих патентов
     * */
    @RequestMapping(value = "/similar-patents", method = RequestMethod.GET)
    public String showSimilarPatents (@RequestParam(value = "id") String patentId, Model model) {
        Long patentIdLong = Long.parseLong(patentId);
        PatentDB currentPatent = patentsRepository.findById(patentIdLong);
        List<PatentDB> patentAnalogs = patentsRepository.findSimilarPatents(patentIdLong);
        model.addAttribute("currentPatent", currentPatent);
        model.addAttribute("patentAnalogs", patentAnalogs);
        model.addAttribute("patentAnalogsCount", patentAnalogs.size());
        return "similar-patents";
    }
}
