package com.samofimp.ChemAnalyzer.controller;

import com.samofimp.ChemAnalyzer.model.PatentDB;
import com.samofimp.ChemAnalyzer.model.PatentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/** Контроллер информации о патенте */
@Controller
public class PatentInfoController {
    /** Репозиторий патентов */
    @Autowired
    PatentsRepository patentsRepository;

    /** Показ страницы с информацией о выбранном патенте
     * @param patentId идентификатор патента
     * @return представление страницы с информацией о выбранном патенте
     * */
    @RequestMapping(value = "/patents/{patentId}", method = RequestMethod.GET)
    public String showPatentInfo(@PathVariable String patentId, Model model) {
        PatentDB patent = patentsRepository.findById(Long.parseLong(patentId, 10));
        int fromPage = Integer.parseInt(patentId) / 10;
        model.addAttribute("fromPage", fromPage);
        model.addAttribute("patent", patent);
        return "patent-info";
    }
}
