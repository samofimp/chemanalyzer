package com.samofimp.ChemAnalyzer.controller;

import com.samofimp.ChemAnalyzer.model.Compound;
import com.samofimp.ChemAnalyzer.model.CompoundDB;
import com.samofimp.ChemAnalyzer.model.CompoundsRepository;
import org.openscience.cdk.exception.CDKException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/** Контроллер схожих химических соединений */
@Controller
public class SimilarCompoundsController {
    /** Репозиторий химических соединений */
    @Autowired
    CompoundsRepository compoundsRepository;

    /** Получение страницы со схожими с выбранным химическими соединениями
     * @param compoundId идентификатор химического соединения
     * @return представление страницы со схожими химическими соединениями
     * */
    @RequestMapping(value = "/similar-compounds", method = RequestMethod.GET)
    public String showSimilarCompounds(@RequestParam(value = "id") String compoundId, Model model) throws SQLException, IOException, CDKException {
        Long compoundIdLong = Long.parseLong(compoundId);
        Compound currentCompound = compoundsRepository.findById(compoundIdLong).FromDatabaseTypes();
        List<CompoundDB> similarCompoundsDB = compoundsRepository.getTopSimilarCompounds(compoundIdLong);
        List<Compound> compounds = new ArrayList<Compound>();
        for (CompoundDB compoundDB : similarCompoundsDB) {
            Compound compound = compoundDB.FromDatabaseTypes();
            if (compound.getSMILES().getFormula().length() > 0)
                compounds.add(compound);
        }
        model.addAttribute("currentCompound", currentCompound);
        model.addAttribute("similarCompoundsCount", compounds.size());
        model.addAttribute("similarCompounds", compounds);
        return "similar-compounds";
    }
}
