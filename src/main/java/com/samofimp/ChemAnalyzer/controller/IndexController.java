package com.samofimp.ChemAnalyzer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
import java.util.HashMap;

/** Контроллер главной страницы */
@Controller
public class IndexController {

    /** Получение главной страницы
     * @return представление главной страницы
     * */
    @GetMapping("/")
    public String index() {
        return "index";
    }
}