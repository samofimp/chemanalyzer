package com.samofimp.ChemAnalyzer.controller;

import com.samofimp.ChemAnalyzer.model.Compound;
import com.samofimp.ChemAnalyzer.model.CompoundDB;
import com.samofimp.ChemAnalyzer.model.PatentDB;
import com.samofimp.ChemAnalyzer.model.PatentsRepository;
import org.openscience.cdk.exception.CDKException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/** Контроллер соединений в патентах */
@Controller
public class CompoundsInPatentController {
    /** Репозиторий патентов */
    @Autowired
    PatentsRepository patentsRepository;

    /** Показ страницы со списком химических соединений в выбранном патенте
     * @param patentId идентификатор патента
     * @return представление страницы со списком химических соединений в выбранном патенте
     * */
    @RequestMapping(value = "/compounds-in-patent", method = RequestMethod.GET)
    public String showCompoundsInPatent(@RequestParam(value = "id") String patentId, Model model) throws SQLException, IOException, CDKException {
        Long patentIdLong = Long.parseLong(patentId);
        PatentDB currentPatent = patentsRepository.findById(patentIdLong);
        List<CompoundDB> compoundDBS = patentsRepository.getCompounds(patentIdLong);
        List<Compound> compounds = new ArrayList<Compound>();
        for (CompoundDB compoundDB : compoundDBS) {
            if (compoundDB.getSMILESRepresentation().length() > 0) {
                compounds.add(compoundDB.FromDatabaseTypes());
            }
        }
        model.addAttribute("currentPatent", currentPatent);
        model.addAttribute("compounds", compounds);
        return "compounds-in-patent";
    }
}
