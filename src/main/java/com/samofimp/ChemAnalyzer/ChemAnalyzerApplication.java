package com.samofimp.ChemAnalyzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChemAnalyzerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChemAnalyzerApplication.class, args);
	}
}
