package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.*;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/** Патент из базы данных */
@Entity
@Table(name = "patent")
public class PatentDB {
    /** Идентификатор */
    @Id
    @GeneratedValue
    Long Id;

    /** Название */
    @Getter
    @Setter
    String Name;

    /** Номер публикации */
    @Getter
    @Setter
    String PublicationNumber;

    /** Дата публикации */
    @Getter
    @Setter
    Date PublicationDate;

    /** Заявитель */
    @Getter
    @Setter
    String Applicant;

    /** Классификация по МПК */
    @Getter
    @Setter
    String IPCClassification;

    /** Получить идентификатор записи */
    public Long getId() {
        return Id;
    }

    /** Установить идентификатор записи */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /** Приведение патента из типов базы данных к внутренним типам*/
    @Transient
    public Patent FromDatabase() {
        Patent patent = new Patent();
        patent.setId(Id);
        patent.setName(Name);
        patent.setPublicationNumber(PublicationNumber);
        patent.setPublicationDate(PublicationDate.toLocalDate());
        patent.setApplicant(Applicant);
        patent.setIPCClassification(IPCClassification);
        return patent;
    }
}
