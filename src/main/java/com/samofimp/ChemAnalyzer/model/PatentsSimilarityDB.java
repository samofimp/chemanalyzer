package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/** Сходство патентов */
@Entity
@Table(name = "patents_similarity")
public class PatentsSimilarityDB {
    /** Идентификатор */
    @Id
    @GeneratedValue
    Long Id;

    /** Идентификатор первого патента */
    @Getter
    @Setter
    Long Patent1_Id;

    /** Идентификатор второго патента */
    @Getter
    @Setter
    Long Patent2_Id;

    /** Мера сходства двух патентов */
    @Getter
    @Setter
    Double Coefficient;

    /** Получение идентификатора */
    public Long getId() {
        return Id;
    }

    /** Установка идентификатора */
    public void setId(Long Id) {
        this.Id = Id;
    }
}
