package com.samofimp.ChemAnalyzer.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialClob;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/** Репозиторий химических соединений */
@Repository
public class CompoundsRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    /** Нахождение всех записей с химическими соединениями из таблицы
     * @return список химических соединений
     * */
    public List<CompoundDB> findAll() throws SQLException {
        String sql = "select * from compound";
        List<CompoundDB> compounds = new ArrayList<CompoundDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            CompoundDB compound = new CompoundDB();
            compound.setId((Long)row.get("id"));
            compound.setSMILESRepresentation(new SerialClob(((String)row.get("smilesname")).toCharArray()));
            compound.setInChiRepresentation(new SerialClob(((String)row.get("inchiname")).toCharArray()));
            compound.setMOLRepresentation(new SerialClob(((String)row.get("molfile")).toCharArray()));
            compound.setStructureRepresentation(new SerialBlob((byte[])row.get("structure")));
            compound.setFingerprintRepresentation(new SerialBlob((byte[])row.get("fingerprint")));
            compounds.add(compound);
        }
        return compounds;
    }

    /** Нахождение записей с химическими соединениями в заданном интервале по идентификаторам
     * @param id1 левая граница поиска
     * @param id2 правая граница поиска
     * @return список химических соединений
     * */
    public List<CompoundDB> findFromTo(Long id1, Long id2) throws SQLException {
        String sql = "select * from compound where id between " + id1.toString() + " and " + id2.toString();
        List<CompoundDB> compounds = new ArrayList<CompoundDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            CompoundDB compound = new CompoundDB();
            compound.setId((Long)row.get("id"));
            compound.setSMILESRepresentation(new SerialClob(((String)row.get("smilesname")).toCharArray()));
            compound.setInChiRepresentation(new SerialClob(((String)row.get("inchiname")).toCharArray()));
            compound.setMOLRepresentation(new SerialClob(((String)row.get("molfile")).toCharArray()));
            compound.setStructureRepresentation(new SerialBlob((byte[])row.get("structure")));
            compound.setFingerprintRepresentation(new SerialBlob((byte[])row.get("fingerprint")));
            compounds.add(compound);
        }
        return compounds;
    }

    /** Нахождение записи с химическим соединением по заданному идентификатору
     * @param id идентификатор
     * @return химическое соединение
     * */
    public CompoundDB findById(Long id) throws SQLException {
        String sql = "select * from compound where id = " + id.toString();
        List<CompoundDB> compounds = new ArrayList<CompoundDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        Map<String, Object> row = rows.get(0);
        CompoundDB compound = new CompoundDB();
        compound.setId((Long)row.get("id"));
        compound.setSMILESRepresentation(new SerialClob(((String)row.get("smilesname")).toCharArray()));
        compound.setInChiRepresentation(new SerialClob(((String)row.get("inchiname")).toCharArray()));
        compound.setMOLRepresentation(new SerialClob(((String)row.get("molfile")).toCharArray()));
        compound.setStructureRepresentation(new SerialBlob((byte[])row.get("structure")));
        compound.setFingerprintRepresentation(new SerialBlob((byte[])row.get("fingerprint")));
        return compound;
    }

    /** Нахождение общего числа химических соединений в таблице
     * @return общее число соединений в таблице
     * */
    public int findTotalCount() {
        String sql = "select count(*) from compound";
        int total = jdbcTemplate.queryForObject(sql, int.class);
        return total;
    }

    /** Удаление химического соединения из таблицы по идентификатору
     * @param id идентификатор соединения
     * */
    public void deleteById(Long id) {
        jdbcTemplate.update("delete from compound where id = " + id.toString());
    }

    /** Вставка химического соединения в таблицу
     * @param compound химическое соединение
     * */
    public void insert(CompoundDB compound) {
        String sql = "insert into compound (smilesname, inchiname, molfile, structure, fingerprint) " +
                "values(?, ?, ?, ?, ?)";
        Object[] params = {compound.getSMILESRepresentation(), compound.getInChiRepresentation(), compound.getMOLRepresentation(), compound.getStructureRepresentation(), compound.getFingerprintRepresentation()};
        int[] types = {Types.CLOB, Types.CLOB, Types.CLOB, Types.BLOB, Types.BLOB};
        jdbcTemplate.update(sql, params, types);
    }

    /** Получение последнего идентификатора записи в таблице
     * @return последний идентификатор
     * */
    public Long getLastId() {
        return jdbcTemplate.queryForObject("select top 1 id from compound order by id desc", Long.class);
    }

    /** Поиск патента с выбранным химическим соединением
     * @param id идентификатор химического соединения
     * @return патент с химических соединением
     * */
    public PatentDB getPatentWithCompound(Long id) {
        String sql = "select patent_id from compound_in_patent where compound_id = " + id.toString();
        Long patentId = jdbcTemplate.queryForObject(sql, Long.class);
        PatentDB patent = new PatentDB();
        patent.setId(jdbcTemplate.queryForObject("select id from patent where id = " + patentId.toString(), Long.class));
        patent.setName(jdbcTemplate.queryForObject("select publication_name from patent where id = " + patentId.toString(), String.class));
        patent.setPublicationNumber(jdbcTemplate.queryForObject("select publication_number from patent where id = " + patentId.toString(), String.class));
        patent.setPublicationDate(jdbcTemplate.queryForObject("select publication_date from patent where id = " + patentId.toString(), Date.class));
        patent.setApplicant(jdbcTemplate.queryForObject("select applicant from patent where id = " + patentId.toString(), String.class));
        patent.setIPCClassification(jdbcTemplate.queryForObject("select ipc_classification from patent where id = " + patentId.toString(), String.class));
        return patent;
    }

    /** Поиск первых 30 наиболее схожих с выбранным химических соединений
     * @param id идентификатор сравниваемого соединения
     * @return список с 30 наиболее схожими соединениями
     * */
    public List<CompoundDB> getTopSimilarCompounds(Long id) throws SQLException {
        String sql = "select top 30 * from compounds_similarity where compound1_id = " + id.toString() + " order by coeff desc";
        List<CompoundsSimilarityDB> similarityDBs = new ArrayList<CompoundsSimilarityDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        if (rows.size() == 0) {
            sql = "select top 30 * from compounds_similarity where compound2_id = " + id.toString() + " order by coeff desc";
            rows = jdbcTemplate.queryForList(sql);
        }
        for (Map<String, Object> row : rows) {
            CompoundsSimilarityDB similarityDB = new CompoundsSimilarityDB();
            similarityDB.setId((Long)row.get("id"));
            similarityDB.setCompound1_Id((Long)row.get("compound1_id"));
            similarityDB.setCompound2_Id((Long)row.get("compound2_id"));
            similarityDB.setCoefficient((Double)row.get("coeff"));
            similarityDBs.add(similarityDB);
        }

        List<CompoundDB> similarCompounds = new ArrayList<CompoundDB>();

        for (CompoundsSimilarityDB similarityDB : similarityDBs) {
            if (similarityDB.getCompound1_Id() == id) {
                similarCompounds.add(findById(similarityDB.getCompound2_Id()));
            }
            else {
                similarCompounds.add(findById(similarityDB.getCompound1_Id()));
            }
        }

        return similarCompounds;
    }
}
