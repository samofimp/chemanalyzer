package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.renderer.AtomContainerRenderer;
import org.openscience.cdk.renderer.font.AWTFontManager;
import org.openscience.cdk.renderer.generators.BasicAtomGenerator;
import org.openscience.cdk.renderer.generators.BasicBondGenerator;
import org.openscience.cdk.renderer.generators.BasicSceneGenerator;
import org.openscience.cdk.renderer.visitor.AWTDrawVisitor;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/** Структурное представление химического соединения */
@NoArgsConstructor
public class CompoundStructureRepresentation {

    /** Структурное представление в изображении */
    @Getter
    @Setter
    Image StructureImage;

    /** Установка структурного представления из MOL-представления
     * @param MOL MOL-представление
     * */
    public void SetStructureImageFromMOL(CompoundMOLRepresentation MOL) {

        AtomContainer molecule = MOL.getMolecule();

        int WIDTH = 1000;
        int HEIGHT = 1000;

        Rectangle drawArea = new Rectangle(WIDTH, HEIGHT);
        Image image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        StructureDiagramGenerator sdg = new StructureDiagramGenerator();
        sdg.setMolecule(molecule);
        try {
            sdg.generateCoordinates();
        } catch (Exception ex) {

        }

        ArrayList generators = new ArrayList();
        generators.add(new BasicSceneGenerator());
        generators.add(new BasicBondGenerator());
        generators.add(new BasicAtomGenerator());

        AtomContainerRenderer renderer = new AtomContainerRenderer(generators, new AWTFontManager());

        renderer.setup(molecule, drawArea);

        Graphics2D g2 = (Graphics2D) image.getGraphics();
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, WIDTH, HEIGHT);

        renderer.paint(molecule, new AWTDrawVisitor(g2));

        this.StructureImage = image;
    }

    /** Сохранение изображения со структурным представлением в файл
     * @param path путь к сохраняемому файлу
     * @return объект сохранённого файла
     * */
    public File SaveImageToFile(String path) throws IOException {
        File file = new File(path);
        ImageIO.write((RenderedImage)this.StructureImage, "PNG", file);
        return file;
    }
}
