package com.samofimp.ChemAnalyzer.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/** Репозиторий патентов */
@Repository
public class PatentsRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    /** Нахождение всех патентов из таблицы
     * @return список патентов
     * */
    public List<PatentDB> findAll() {
        String sql = "select * from patent";
        List<PatentDB> patents = new ArrayList<PatentDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            PatentDB patent = new PatentDB();
            patent.setId((Long)row.get("id"));
            patent.setName((String)row.get("publication_name"));
            patent.setPublicationNumber((String)row.get("publication_number"));
            patent.setPublicationDate((Date)row.get("publication_date"));
            patent.setApplicant((String)row.get("applicant"));
            patent.setIPCClassification((String)row.get("ipc_classification"));
            patents.add(patent);
        }
        return patents;
    }

    /** Нахождение патентов из таблицы в заданном интервале по идентификаторам
     * @param id1 левая граница поиска
     * @param id2 правая граница поиска
     * @return список патентов
     * */
    public List<PatentDB> findFromTo(Long id1, Long id2) {
        String sql = "select * from patent where id between " + id1.toString() + " and " + id2.toString();
        List<PatentDB> patents = new ArrayList<PatentDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            PatentDB patent = new PatentDB();
            patent.setId((Long)row.get("id"));
            patent.setName((String)row.get("publication_name"));
            patent.setPublicationNumber((String)row.get("publication_number"));
            patent.setPublicationDate((Date)row.get("publication_date"));
            patent.setApplicant((String)row.get("applicant"));
            patent.setIPCClassification((String)row.get("ipc_classification"));
            patents.add(patent);
        }
        return patents;
    }

    /** Нахождение патента из таблицы по заданному идентификатору
     * @param id идентификатор патента
     * @return патент
     * */
    public PatentDB findById(Long id) {
        PatentDB patent = new PatentDB();
        patent.setId(jdbcTemplate.queryForObject("select id from patent where id = " + id.toString(), Long.class));
        patent.setName(jdbcTemplate.queryForObject("select publication_name from patent where id = " + id.toString(), String.class));
        patent.setPublicationNumber(jdbcTemplate.queryForObject("select publication_number from patent where id = " + id.toString(), String.class));
        patent.setPublicationDate(jdbcTemplate.queryForObject("select publication_date from patent where id = " + id.toString(), Date.class));
        patent.setApplicant(jdbcTemplate.queryForObject("select applicant from patent where id = " + id.toString(), String.class));
        patent.setIPCClassification(jdbcTemplate.queryForObject("select ipc_classification from patent where id = " + id.toString(), String.class));
        return patent;
    }

    /** Нахождение общего числа патентов в таблице
     * @return общее число патентов в таблице
     * */
    public int findTotalCount() {
        String sql = "select count(*) from patent";
        int total = jdbcTemplate.queryForObject(sql, int.class);
        return total;
    }

    /** Удаление патента из таблицы по заданному идентификатору
     * @param id идентификатор удаляемого патента
     * */
    public void deleteById(Long id) {
        jdbcTemplate.update("delete from patent where id = " + id.toString());
    }

    /** Вставка патента в таблицу
     * @param patent патент
     * */
    public void insert(PatentDB patent) {
        String sql = "insert into patent (publication_name, publication_number, publication_date, applicant, ipc_classification) " +
                "values(?, ?, ?, ?, ?)";
        Object[] params = {patent.getName(), patent.getPublicationNumber(), patent.getPublicationDate(), patent.getApplicant(), patent.getIPCClassification()};
        int[] types = {Types.VARCHAR, Types.VARCHAR, Types.DATE, Types.VARCHAR, Types.VARCHAR};
        jdbcTemplate.update(sql, params, types);
    }

    /** Получение последнего идентификатора из таблицы
     * @return последний идентификатор
     * */
    public Long getLastId() {
        return jdbcTemplate.queryForObject("select top 1 id from patent order by id desc", Long.class);
    }

    /** Получение списка химических соединений в патенте из таблицы
     * @param id идентификатор патента
     * @return список химических соединений в патенте
     * */
    public List<CompoundDB> getCompounds(Long id) {
        String sql = "select * from compound_in_patent where patent_id = " + id.toString();
        List<CompoundInPatentDB> compoundsInPatents = new ArrayList<CompoundInPatentDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            CompoundInPatentDB compoundInPatent = new CompoundInPatentDB();
            compoundInPatent.setId((Long)row.get("id"));
            compoundInPatent.setCompound_Id((Long)row.get("compound_id"));
            compoundInPatent.setPatent_Id((Long)row.get("patent_id"));
            compoundsInPatents.add(compoundInPatent);
        }

        List<CompoundDB> compounds = new ArrayList<CompoundDB>();
        for (int i = 0; i < compoundsInPatents.size(); i++) {
            Long nextId = compoundsInPatents.get(i).getCompound_Id();
            CompoundDB compound = new CompoundDB();
            compound.setId(jdbcTemplate.queryForObject("select id from compound where id = " + nextId.toString(), Long.class));
            compound.setSMILESRepresentation(jdbcTemplate.queryForObject("select smilesname from compound where id = " + nextId.toString(), Clob.class));
            compound.setInChiRepresentation(jdbcTemplate.queryForObject("select inchiname from compound where id = " + nextId.toString(), Clob.class));
            compound.setMOLRepresentation(jdbcTemplate.queryForObject("select molfile from compound where id = " + nextId.toString(), Clob.class));
            compound.setStructureRepresentation(jdbcTemplate.queryForObject("select structure from compound where id = " + nextId.toString(), Blob.class));
            compound.setFingerprintRepresentation(jdbcTemplate.queryForObject("select fingerprint from compound where id = " + nextId.toString(), Blob.class));
            compounds.add(compound);
        }
        return compounds;
    }

    /** Нахождение схожих с выбранным патентов
     * @param id идентификатор патента
     * @return список схожих патентов
     * */
    public List<PatentDB> findSimilarPatents(Long id) {
        String sql = "select * from patents_similarity where patent1_id = " + id.toString() + " order by coeff desc";
        List<PatentsSimilarityDB> similarityDBs = new ArrayList<PatentsSimilarityDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        if (rows.size() == 0) {
            sql = "select * from patents_similarity where patent2_id = " + id.toString() + " order by coeff desc";
            rows = jdbcTemplate.queryForList(sql);
        }
        for (Map<String, Object> row : rows) {
            PatentsSimilarityDB similarityDB = new PatentsSimilarityDB();
            similarityDB.setId((Long)row.get("id"));
            similarityDB.setPatent1_Id((Long)row.get("patent1_id"));
            similarityDB.setPatent2_Id((Long)row.get("patent2_id"));
            similarityDB.setCoefficient((Double)row.get("coeff"));
            similarityDBs.add(similarityDB);
        }

        List<PatentDB> similarPatents = new ArrayList<PatentDB>();

        for (PatentsSimilarityDB similarityDB : similarityDBs) {
            if (similarityDB.getPatent1_Id() == id) {
                similarPatents.add(findById(similarityDB.getPatent2_Id()));
            }
            else {
                similarPatents.add(findById(similarityDB.getPatent1_Id()));
            }
        }

        return similarPatents;
    }
}
