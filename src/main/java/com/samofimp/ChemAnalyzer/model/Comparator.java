package com.samofimp.ChemAnalyzer.model;

import lombok.NoArgsConstructor;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.fingerprint.BitSetFingerprint;
import org.openscience.cdk.similarity.Tanimoto;

import java.util.BitSet;

/** Класс сравнения химических соединений на структурное сходство */
@NoArgsConstructor
public class Comparator {

    /** Сравнение химических соединений
     * @param compound1 первое соединение для сравнения
     * @param compound2 второе соединение для сравнения
     * @return мера сходства химических соединений (от 0 до 1)
     * */
    public static double compareCompounds(Compound compound1, Compound compound2) throws CDKException {

        // Получение набора бит, представляющих отпечаток молекулы
        BitSet fingerprint1 = compound1.getFingerprint().getFingerprint();
        BitSet fingerprint2 = compound2.getFingerprint().getFingerprint();
        int size1 = fingerprint1.size();
        int size2 = fingerprint2.size();
        BitSetFingerprint bitSetFingerprint1;
        BitSetFingerprint bitSetFingerprint2;

        // Приведение наборов бит к одинаковому размеру
        if (size1 > size2) {
            BitSet newFingerprint = new BitSet(size1);
            newFingerprint.or(fingerprint2);
            bitSetFingerprint1 = new BitSetFingerprint(fingerprint1);
            bitSetFingerprint2 = new BitSetFingerprint(newFingerprint);
        }
        else if (size1 < size2) {
            BitSet newFingerprint = new BitSet(size2);
            newFingerprint.or(fingerprint1);
            bitSetFingerprint1 = new BitSetFingerprint(newFingerprint);
            bitSetFingerprint2 = new BitSetFingerprint(fingerprint2);
        }
        else {
            bitSetFingerprint1 = new BitSetFingerprint(fingerprint1);
            bitSetFingerprint2 = new BitSetFingerprint(fingerprint2);
        }

        return Tanimoto.calculate(bitSetFingerprint1, bitSetFingerprint2);
    }
}
