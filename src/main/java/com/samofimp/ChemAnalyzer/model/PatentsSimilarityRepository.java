package com.samofimp.ChemAnalyzer.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/** Репозиторий сходств патентов */
@Repository
public class PatentsSimilarityRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    /** Нахождение всех записей со сходствами патентов из таблицы
     * @return список записей со сходствами патентов
     * */
    public List<PatentsSimilarityDB> findAll() {
        String sql = "select * from patents_similarity";
        List<PatentsSimilarityDB> patentsSimilarities = new ArrayList<PatentsSimilarityDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            PatentsSimilarityDB patentsSimilarity = new PatentsSimilarityDB();
            patentsSimilarity.setId((Long)row.get("id"));
            patentsSimilarity.setPatent1_Id((Long)row.get("patent1_id"));
            patentsSimilarity.setPatent2_Id((Long)row.get("patent2_id"));
            patentsSimilarity.setCoefficient((Double)row.get("coeff"));
            patentsSimilarities.add(patentsSimilarity);
        }
        return patentsSimilarities;
    }

    /** Нахождение записей со сходствами патентов из таблицы в заданном интервале по идентификаторам
     * @param id1 левая граница поиска
     * @param id2 правая граница поиска
     * @return список записей со сходствами патентов
     * */
    public List<PatentsSimilarityDB> findFromTo(Long id1, Long id2) {
        String sql = "select * from patents_similarity where id between" + id1.toString() + " and " + id2.toString();
        List<PatentsSimilarityDB> patentsSimilarities = new ArrayList<PatentsSimilarityDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            PatentsSimilarityDB patentsSimilarity = new PatentsSimilarityDB();
            patentsSimilarity.setId((Long)row.get("id"));
            patentsSimilarity.setPatent1_Id((Long)row.get("patent1_id"));
            patentsSimilarity.setPatent2_Id((Long)row.get("patent2_id"));
            patentsSimilarity.setCoefficient((Double)row.get("coeff"));
            patentsSimilarities.add(patentsSimilarity);
        }
        return patentsSimilarities;
    }

    /** Нахождение записи со сходством патентов по заданному идентификатору
     * @param id идентификатор
     * @return сходство патентов
     * */
    public PatentsSimilarityDB findById(Long id) {
        PatentsSimilarityDB patentsSimilarity = new PatentsSimilarityDB();
        patentsSimilarity.setId(jdbcTemplate.queryForObject("select id from patents_similarity where id = " + id.toString(), Long.class));
        patentsSimilarity.setPatent1_Id(jdbcTemplate.queryForObject("select patent1_id from patents_similarity where id = " + id.toString(), Long.class));
        patentsSimilarity.setPatent2_Id(jdbcTemplate.queryForObject("select patent2_id from patents_similarity where id = " + id.toString(), Long.class));
        patentsSimilarity.setCoefficient(jdbcTemplate.queryForObject("select coeff from patents_similarity where id = " + id.toString(), Double.class));
        return patentsSimilarity;
    }

    /** Нахождение общего числа записей из таблицы со сходствами патентов
     * @return общее число записей*/
    public int findTotalCount() {
        String sql = "select count(*) from patents_similarity";
        int total = jdbcTemplate.queryForObject(sql, int.class);
        return total;
    }

    /** Удаление записи со сходством патентов из таблицы по заданному идентификатору
     * @param id идентификатор
     * */
    public void deleteById(Long id) {
        jdbcTemplate.update("delete from patents_similarity where id = " + id.toString());
    }

    /** Вставка записи со сходством патентов в таблицу
     * @param patentsSimilarity сходство патентов
     * */
    public void insert(PatentsSimilarityDB patentsSimilarity) {
        String sql = "insert into patents_similarity (patent1_id, patent2_id, coeff) " +
                "values(?, ?, ?)";
        Object[] params = {patentsSimilarity.getPatent1_Id(), patentsSimilarity.getPatent2_Id(), patentsSimilarity.getCoefficient()};
        int[] types = {Types.BIGINT, Types.BIGINT, Types.DOUBLE};
        jdbcTemplate.update(sql, params, types);
    }

    /** Получение последнего идентификатора из таблицы со сходствами патентов
     * @return последний идентификатор
     * */
    public Long getLastId() {
        return jdbcTemplate.queryForObject("select top 1 id from patents_similarity order by id desc", Long.class);
    }
}
