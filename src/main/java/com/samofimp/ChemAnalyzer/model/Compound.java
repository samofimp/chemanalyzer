package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.awt.image.RenderedImage;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialClob;

/** Химическое соединение */
@NoArgsConstructor
public class Compound {

    /** Идентификатор соединения в базе данных */
    @Getter
    @Setter
    Long Id;

    /** SMILES-представление соединения */
    @Getter
    @Setter
    CompoundSMILESRepresentation SMILES;

    /** InChi-представление соединения */
    @Getter
    @Setter
    CompoundInChiRepresentation InChi;

    /** MOL-представление соединения */
    @Getter
    @Setter
    CompoundMOLRepresentation MOL;

    /** Структурное представление соединения */
    @Getter
    @Setter
    CompoundStructureRepresentation Structure;

    /** Отпечаток молекулы */
    @Getter
    @Setter
    CompoundFingerprintRepresentation Fingerprint;

    /** Проверка - имеет ли формула радикалы
     * @return boolean - имеет ли формула радикалы
     * */
    public boolean CheckIfHasRadicals() {
        if (this.SMILES.getFormula().indexOf('*') == -1)
            return false;
        else
            return true;
    }

    /** Сравнение текущего соединения с другим по SMILES-представлению
     * @param compound второе соединение
     * @return одинаковы ли формулы*/
    public boolean equals(Compound compound) {
        return (this.SMILES.getFormula().equals(compound.getSMILES().getFormula()));
    }

    /** Приведение текущего соединения к типам базы данных
     * @return соединение в типах базы данных
     * */
    public CompoundDB ToDatabaseTypes() throws IOException, SQLException {
        CompoundDB compoundDB = new CompoundDB();
        compoundDB.setSMILESRepresentation(SMILESToClob(SMILES));
        compoundDB.setInChiRepresentation(InChiToClob(InChi));
        compoundDB.setMOLRepresentation(MOLToClob(MOL));
        compoundDB.setStructureRepresentation(StructureToBlob(Structure));
        compoundDB.setFingerprintRepresentation(FingerprintToBlob(Fingerprint));
        return compoundDB;
    }

    /** Перевод SMILES-типа в Clob-тип для базы данных
     * @param SMILES SMILES-представление соединения
     * @return SMILES в Clob-типе
     * */
    private Clob SMILESToClob(CompoundSMILESRepresentation SMILES) throws SQLException {
        Clob targetClob = new SerialClob(SMILES.getFormula().toCharArray());
        return targetClob;
    }

    /** Перевод InChi-типа в Clob-тип для базы данных
     * @param InChi InChi-представление соединения
     * @return InChi в Clob-типе
     * */
    private Clob InChiToClob(CompoundInChiRepresentation InChi) throws SQLException {
        Clob targetClob;
        try {
            targetClob = new SerialClob(InChi.getFormula().toCharArray());
        }
        catch (NullPointerException e) {
            targetClob = new SerialClob(" ".toCharArray());
        }
        return targetClob;
    }

    /** Перевод MOL-типа в Clob-тип для базы данных
     * @param MOL MOL-представление соединения
     * @return MOL в Clob-типе
     * */
    private Clob MOLToClob(CompoundMOLRepresentation MOL) throws IOException, SQLException {
        Clob targetClob = new SerialClob(MOL.getMOLInString().toCharArray());
        return targetClob;
    }

    /** Перевод изображения со структурой в Blob-тип для базы данных
     * @param Structure структурное представление соединения
     * @return структура в Blob-типе
     * */
    private Blob StructureToBlob(CompoundStructureRepresentation Structure) throws IOException, SQLException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write((RenderedImage)Structure.getStructureImage(), "png", os);
        Blob targetBlob = new SerialBlob(os.toByteArray());
        return targetBlob;
    }

    /** Перевод набора бит с отпечатком молекулы в Blob-тип для базы данных
     * @param Fingerprint отпечаток молекулы
     * @return отпечаток молекулы в Blob-типе
     * */
    private Blob FingerprintToBlob(CompoundFingerprintRepresentation Fingerprint) throws SQLException {
        Blob targetBlob = new SerialBlob(Fingerprint.getFingerprint().toByteArray());
        return targetBlob;
    }


}
