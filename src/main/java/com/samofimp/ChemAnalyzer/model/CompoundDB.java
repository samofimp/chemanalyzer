package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.Setter;
import javax.imageio.ImageIO;
import javax.persistence.Id;

import org.openscience.cdk.exception.CDKException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import java.awt.Image;
import java.io.*;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Map;

/** Химическое соединение в типах базы данных */
@Entity
@Table(name = "compound")
public class CompoundDB {

    /** Идентификатор */
    @Id
    @GeneratedValue
    Long Id;

    /** SMILES-представление */
    @Getter
    @Setter
    Clob SMILESRepresentation;

    /** InChi-представление */
    @Getter
    @Setter
    Clob InChiRepresentation;

    /** MOL-представление */
    @Getter
    @Setter
    Clob MOLRepresentation;

    /** Структурное представление */
    @Getter
    @Setter
    Blob StructureRepresentation;

    /** Отпечаток молекулы */
    @Getter
    @Setter
    Blob FingerprintRepresentation;

    /** Получение идентификатора */
    public Long getId() {
        return Id;
    }

    /** Установка значения идентификатора */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /** Перевод химического соединения во внутренние типы из типов базы данных
     * @return химическое соединение во внутренних типах
     * */
    @Transient
    public Compound FromDatabaseTypes() throws IOException, CDKException, SQLException {
        Compound compound = new Compound();
        compound.setId(Id);
        compound.setSMILES(SMILESFromClob(SMILESRepresentation));
        compound.setInChi(InChiFromClob(InChiRepresentation));
        compound.setMOL(MOLFromClob(MOLRepresentation));
        compound.setStructure(StructureFromBlob(StructureRepresentation));
        compound.setFingerprint(FingerprintFromBlob(FingerprintRepresentation));
        return compound;
    }

    /** Перевод SMILES-представления из Clob во внутренний тип
     * @param clob SMILES-представление в Clob
     * @return SMILES-представление во внутреннем типе
     * */
    @Transient
    private CompoundSMILESRepresentation SMILESFromClob(Clob clob) throws SQLException {
        String targetString;
        if (clob.length() < 1) {
            targetString ="";
        }
        else {
            targetString = clob.getSubString(1, (int)clob.length());
        }
        CompoundSMILESRepresentation representation = new CompoundSMILESRepresentation();
        representation.setFormula(targetString);
        return representation;
    }

    /** Перевод InChi-представления из Clob во внутренний тип
     * @param clob InChi-представление в Clob
     * @return InChi-представление во внутреннем типе
     * */
    @Transient
    private CompoundInChiRepresentation InChiFromClob(Clob clob) throws SQLException {
        String targetString;
        if (clob.length() < 1) {
            targetString = "";
        }
        else {
            targetString = clob.getSubString(1, (int) clob.length());
        }
        CompoundInChiRepresentation representation = new CompoundInChiRepresentation();
        representation.setFormula(targetString);
        return representation;
    }

    /** Перевод MOL-представления из Clob во внутренний тип
     * @param clob MOL-представление в Clob
     * @return MOL-представление во внутреннем типе
     * */
    @Transient
    private CompoundMOLRepresentation MOLFromClob(Clob clob) throws CDKException, SQLException, IOException {
        CompoundMOLRepresentation representation = new CompoundMOLRepresentation();
        String MOLString;
        if (clob.length() < 1) {
            MOLString = "";
        }
        else {
            MOLString = clob.getSubString(1, (int) clob.length());
        }
        representation.setMOLInString(MOLString);
        representation.SetMoleculeFromReader(new StringReader(MOLString));
        return representation;
    }

    /** Перевод структуры молекулы из Blob во внутренний тип
     * @param blob структурное представление в Blob
     * @return структурное представление во внутреннем типе
     * */
    @Transient
    private CompoundStructureRepresentation StructureFromBlob(Blob blob) throws IOException, SQLException {
        InputStream inputStream = blob.getBinaryStream();
        Image image = ImageIO.read(inputStream);
        CompoundStructureRepresentation representation = new CompoundStructureRepresentation();
        representation.setStructureImage(image);
        return representation;
    }

    /** Перевод отпечатка молекулы из Blob во внутренний тип
     * @param blob отпечаток молекулы в Blob
     * @return отпечаток молекулы во внутреннем типе
     * */
    @Transient
    private CompoundFingerprintRepresentation FingerprintFromBlob(Blob blob) throws SQLException {
        byte[] byteArray;
        if (blob.length() < 1) {
            byteArray = new byte[1];
        }
        else {
            byteArray = blob.getBytes(1, (int) blob.length());
        }
        BitSet fingerprint = BitSet.valueOf(byteArray);
        CompoundFingerprintRepresentation representation = new CompoundFingerprintRepresentation();
        representation.setFingerprint(fingerprint);
        return representation;
    }
}
