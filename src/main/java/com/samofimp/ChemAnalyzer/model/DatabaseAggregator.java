package com.samofimp.ChemAnalyzer.model;

import lombok.NoArgsConstructor;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.openscience.cdk.exception.CDKException;
import org.springframework.beans.factory.annotation.Autowired;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** Сбор данных о химических соединениях и патентах в базу данных */
@Service
public class DatabaseAggregator {

    /** Репозиторий с химическими соединениями */
    @Autowired
    CompoundsRepository compoundsRepository;
    /** Репозиторий с патентами */
    @Autowired
    PatentsRepository patentsRepository;
    /** Репозиторий с химическими соединениями в патентах */
    @Autowired
    CompoundsInPatentsRepository compoundsInPatentsRepository;
    /** Репозиторий со сходством химических соединений */
    @Autowired
    CompoundsSimilarityRepository compoundsSimilarityRepository;
    /** Репозиторий со сходством патентов */
    @Autowired
    PatentsSimilarityRepository patentsSimilarityRepository;

    /** Директория с разархивированными патентами */
    String patentsDirectory;
    /** Прогресс сбора данных (от 0 до 100) */
    float aggregationProgress = 0;

    /** Запуск сбора данных
     * @param directory директория с разархивированными патентами
     * */
    public void launch(String directory) throws IOException, SQLException, CDKException, ParserConfigurationException, SAXException {
        this.patentsDirectory = directory;
        // Поиск внутренних директорий
        File unpackedDir = new File(patentsDirectory);
        ArrayList<String> innerFolders = new ArrayList<String>(Arrays.asList(unpackedDir.list()));
        int count = innerFolders.size();
        Long firstId = null;
        Long lastId = null;

        // Обработка каждого патента - занесение информации в БД о патенте и химическом соединении
        float percentOnPatent = (float)30 / count;
        for (int i = 0; i < count; i++) {
            Long patentId = null;
            List<Long> compoundsIds = new ArrayList<Long>();
            File innerFolder = new File(patentsDirectory + "/" + innerFolders.get(i));
            ArrayList<String> innerFiles = new ArrayList<String>(Arrays.asList(innerFolder.list()));
            for (int j = 0; j < innerFiles.size(); j++) {
                int pointIndex = innerFiles.get(j).indexOf('.');
                if (pointIndex == -1)
                    continue;
                String substring = innerFiles.get(j).substring(pointIndex);
                if (substring.equals(".xml") || substring.equals(".XML")) {
                    Patent currentPatent = getPatentInfo(patentsDirectory + "/" + innerFolders.get(i) + "/" + innerFiles.get(j));
                    if (currentPatent == null)
                        continue;
                    PatentDB currentPatentDB = currentPatent.ToDatabase();
                    patentsRepository.insert(currentPatentDB);
                    patentId = patentsRepository.getLastId();
                    if (i == 0)
                        firstId = patentId;
                    else if (i == count - 1)
                        lastId = patentId;
                }
                else if (substring.equals(".mol") || substring.equals(".MOL")) {
                    Compound currentCompound = getCompoundInfo(patentsDirectory + "/" + innerFolders.get(i) + "/" + innerFiles.get(j));
                    if (currentCompound == null)
                        continue;
                    CompoundDB currentCompoundDB = currentCompound.ToDatabaseTypes();
                    compoundsRepository.insert(currentCompoundDB);
                    compoundsIds.add(compoundsRepository.getLastId());
                }
            }
            if (patentId != null) {
                for (int j = 0; j < compoundsIds.size(); j++) {
                    CompoundInPatentDB compoundInPatentDB = new CompoundInPatentDB();
                    compoundInPatentDB.setPatent_Id(patentId);
                    compoundInPatentDB.setCompound_Id(compoundsIds.get(j));
                    compoundsInPatentsRepository.insert(compoundInPatentDB);
                }
            }
            aggregationProgress += percentOnPatent;
        }
        FileUtils.deleteDirectory(unpackedDir);

        // Поиск схожих соединений и сравнение патентов
        percentOnPatent = (float)70 / (lastId - firstId);
        for (Long i = firstId; i < lastId; i++) {
            //PatentDB patentDB1 = patentsRepository.findById(i);
            List<CompoundDB> compoundDBs1 = patentsRepository.getCompounds(i);
            for (Long j = new Long(1); j < lastId; j++) {
                if (i == j)
                    continue;
                //PatentDB patentDB2 = patentsRepository.findById(j);
                List<CompoundDB> compoundDBs2 = patentsRepository.getCompounds(j);
                Double coefficient = comparePatents(compoundDBs1, compoundDBs2);
                if (coefficient > 0.01) {
                    PatentsSimilarityDB patentsSimilarity = new PatentsSimilarityDB();
                    patentsSimilarity.setPatent1_Id(i);
                    patentsSimilarity.setPatent2_Id(j);
                    patentsSimilarity.setCoefficient(coefficient);
                    patentsSimilarityRepository.insert(patentsSimilarity);
                }
                aggregationProgress += percentOnPatent;
            }
        }
        aggregationProgress = 100;
    }

    /** Получение информации о патенте из файла
     * @param directory директория с патентом
     * @return извлечённый патент
     * */
    private Patent getPatentInfo(String directory) throws IOException, SAXException, ParserConfigurationException {
        Patent patent = new Patent();
        File inputFile = new File(directory);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        dBuilder.setEntityResolver(new EntityResolver() {
            @Override
            public InputSource resolveEntity(String publicId, String systemId)
                    throws SAXException, IOException {
                if (systemId.contains(".dtd") || systemId.contains(".DTD")) {
                    return new InputSource(new StringReader(""));
                } else {
                    return null;
                }
            }
        });
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();

        Element PublicationName = (Element)doc.getElementsByTagName("invention-title").item(0);
        try {
            patent.setName(PublicationName.getTextContent());
        }
        catch (NullPointerException e) {
            return null;
        }

        Element PublicationNumber = (Element)doc.getElementsByTagName("publication-reference").item(0);
        try {
            patent.setPublicationNumber(PublicationNumber.getElementsByTagName("country").item(0).getTextContent() + PublicationNumber.getElementsByTagName("doc-number").item(0).getTextContent());
        }
        catch (NullPointerException e) {
            patent.setPublicationNumber("No number");
        }

        Element PublicationDate = (Element)doc.getElementsByTagName("us-patent-grant").item(0);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        try {
            String date = PublicationDate.getAttribute("date-publ");
            patent.setPublicationDate(LocalDate.parse(date, formatter));
        }
        catch (NullPointerException e) {
            patent.setPublicationDate(LocalDate.parse("20000101", formatter));
        }

        NodeList applicants = doc.getElementsByTagName("orgname");
        String applicantsString = "";
        for (int i = 0; i < applicants.getLength(); i++) {
            Node applicant = applicants.item(i);
            applicantsString += applicant.getTextContent() + "; ";
        }
        try {
            patent.setApplicant(applicantsString);
        }
        catch (NullPointerException e) {
            patent.setApplicant("No applicants");
        }

        // Парсинг патента по тегам
        NodeList sections = doc.getElementsByTagName("section");
        NodeList classes = doc.getElementsByTagName("class");
        NodeList subclasses = doc.getElementsByTagName("subclass");
        NodeList maingroups = doc.getElementsByTagName("main-group");
        NodeList subgroups = doc.getElementsByTagName("subgroup");
        String IPCString = "";
        for (int i = 0; i < sections.getLength(); i++) {
            IPCString += sections.item(i).getTextContent() + classes.item(i).getTextContent() + subclasses.item(i).getTextContent()
                    + " " + maingroups.item(i).getTextContent() + "/" + subgroups.item(i).getTextContent() + "; ";
        }
        try {
            patent.setIPCClassification(IPCString);
        }
        catch (NullPointerException e) {
            patent.setIPCClassification("No classification");
        }

        return patent;
    }

    /** Получение информации о химическом соединении из файла
     * @param directory путь к химическому соединению
     * @return химическое соединение
     * */
    private Compound getCompoundInfo(String directory) throws IOException, CDKException {
        Compound compound = new Compound();
        CompoundMOLRepresentation mol = new CompoundMOLRepresentation();
        try {
            mol.setMoleculeFromPath(directory);
            compound.setMOL(mol);
            CompoundSMILESRepresentation smiles = new CompoundSMILESRepresentation();
            smiles.setFromMOL(mol);
            compound.setSMILES(smiles);
            CompoundInChiRepresentation inchi = new CompoundInChiRepresentation();
            if (compound.CheckIfHasRadicals()) {
                inchi.setFormula("");
            }
            else {
                inchi.setFromMOL(mol);
            }
            compound.setInChi(inchi);
            CompoundStructureRepresentation structure = new CompoundStructureRepresentation();
            structure.SetStructureImageFromMOL(mol);
            compound.setStructure(structure);
            CompoundFingerprintRepresentation fingerprint = new CompoundFingerprintRepresentation();
            fingerprint.SetFromMOL(mol);
            compound.setFingerprint(fingerprint);
        }
        catch (NullPointerException e) {
            compound = null;
        }
        catch (CDKException e) {
            compound = null;
        }
        catch (ArrayIndexOutOfBoundsException e) {
            compound = null;
        }
        catch (IndexOutOfBoundsException e) {
            compound = null;
        }
        catch (IllegalArgumentException e) {
            compound = null;
        }
        return compound;
    }

    /** Сравнение двух патентов
     * @param compounds1 список химических соединений из первого патента
     * @param compounds2 список химических соединений из второго патента
     * @return мера сходства двух выбранных патентов
     * */
    private Double comparePatents(List<CompoundDB> compounds1, List<CompoundDB> compounds2) throws SQLException, IOException, CDKException {
        Double max = 0.0;
        for (CompoundDB compoundDB1 : compounds1) {
            for (CompoundDB compoundDB2 : compounds2) {
                try {
                    Compound compound1 = compoundDB1.FromDatabaseTypes();
                    Compound compound2 = compoundDB2.FromDatabaseTypes();
                    if (compound1.getId() == compound2.getId())
                        continue;
                    Double coefficient = Comparator.compareCompounds(compound1, compound2);
                    if (coefficient > 0.1) {
                        CompoundsSimilarityDB compoundsSimilarity = new CompoundsSimilarityDB();
                        compoundsSimilarity.setCompound1_Id(compoundDB1.getId());
                        compoundsSimilarity.setCompound2_Id(compoundDB2.getId());
                        compoundsSimilarity.setCoefficient(coefficient);
                        compoundsSimilarityRepository.insert(compoundsSimilarity);
                        if (coefficient > max)
                            max = coefficient;
                    }
                }
                catch (SQLException e) {
                    continue;
                }
            }
        }
        return max;
    }

    /** Получение процесса сбора данных
     * @return прогресс сбора данных
     * */
    public int getAggregationProgress() {
        return (int)Math.floor(aggregationProgress);
    }
}
