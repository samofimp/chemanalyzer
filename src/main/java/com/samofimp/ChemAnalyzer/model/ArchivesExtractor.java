package com.samofimp.ChemAnalyzer.model;


import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.utils.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.lang.Math;

/** Класс разархивирования патентного массива */
public class ArchivesExtractor {

    /** Директория с архивами */
    String archivesDirectory;
    /** Список архивов в директории */
    ArrayList<String> archivesList;
    /** Прогресс разархивирования от 1 до 100 */
    float extractionProgress = 0;

    /** Конструктор класса ArchivesExtractor
     * @param directory директория с архивами
     * @param archives список архивов в директории
     * */
    public ArchivesExtractor(String directory, ArrayList<String> archives) {
        archivesDirectory = directory;
        archivesList = archives;
    }

    /** Запуск разархивирования */
    public void launch() throws IOException {

        // Распаковка внешних архивов
        File tmpdir = new File(archivesDirectory + "/tmp");
        if (tmpdir.exists())
            FileUtils.deleteDirectory(tmpdir);
        tmpdir.mkdir();
        Iterator<String> iter = archivesList.iterator();
        while (iter.hasNext()) {
            String nextArchive = iter.next();
            int pointIndex = nextArchive.lastIndexOf(".");
            String substring = nextArchive.substring(pointIndex);
            if (substring.equals(".tar"))
                decompressTar(archivesDirectory + '/' + nextArchive, tmpdir);
            else if (substring.equals(".zip") || substring.equals(".ZIP"))
                decompressZip(archivesDirectory + '/' + nextArchive, tmpdir);
        }
        extractionProgress = 5;

        // Поиск внутренних директорий и их распаковка архивов
        ArrayList<String> innerFolders = new ArrayList<String>(Arrays.asList(tmpdir.list()));
        ArrayList<File> innerFoldersAsFiles = new ArrayList<File>();
        for (int i = 0; i < innerFolders.size(); i++) {
            if (innerFolders.get(i).contains("-SUPP")) {
                FileUtils.deleteDirectory(new File(tmpdir, innerFolders.get(i)));
                innerFolders.remove(i);
                i--;
            }
            else {
                innerFoldersAsFiles.add(new File(tmpdir, innerFolders.get(i)));
            }
        }

        ArrayList<File> UTILFoldersAsFiles = new ArrayList<File>();
        for (int i = 0; i < innerFoldersAsFiles.size(); i++) {

            ArrayList<String> tmp = new ArrayList<String>(Arrays.asList(innerFoldersAsFiles.get(i).list()));
            for (int j = 0; j < tmp.size(); j++) {
                if (!tmp.get(j).contains("UTIL")) {
                    FileUtils.deleteDirectory(new File(innerFoldersAsFiles.get(i), tmp.get(j)));
                }
                else {
                    UTILFoldersAsFiles.add(new File(innerFoldersAsFiles.get(i), tmp.get(j)));
                }
            }
        }
        extractionProgress = 10;

        File unpackedDir = new File(archivesDirectory + "/unpacked");
        float percentsOnFolder = (float)80 / UTILFoldersAsFiles.size();
        for (int i = 0; i < UTILFoldersAsFiles.size(); i++) {
            ArrayList<String> zipArchives = new ArrayList<String>(Arrays.asList(UTILFoldersAsFiles.get(i).list()));
            Iterator<String> iterator = zipArchives.iterator();
            while (iterator.hasNext()) {
                File nextZip = new File(UTILFoldersAsFiles.get(i), iterator.next());
                decompressZip(nextZip.getPath(), unpackedDir);
            }
            FileUtils.deleteDirectory(UTILFoldersAsFiles.get(i));
            extractionProgress += percentsOnFolder;
        }

        // Удаление каталогов, не содержащих .MOL файлы
        ArrayList<String> patents = new ArrayList<String>(Arrays.asList(unpackedDir.list()));
        percentsOnFolder = (float)10 / patents.size();
        for (int i = 0; i < patents.size(); i++) {
            boolean contains = false;
            File patent = new File(unpackedDir, patents.get(i));
            ArrayList<String> filesInPatent = new ArrayList<String>(Arrays.asList(patent.list()));
            Iterator<String> iterator = filesInPatent.iterator();
            while (iterator.hasNext()) {
                String currentName = iterator.next();
                int pointIndex = currentName.lastIndexOf(".");

                if (pointIndex < 0)
                    continue;
                String substring = currentName.substring(pointIndex);
                if (substring.equals(".MOL") || substring.equals(".mol")) {
                    contains = true;
                }
            }
            if (!contains) {
                FileUtils.deleteDirectory(patent);
            }
            extractionProgress += percentsOnFolder;
        }
        FileUtils.deleteDirectory(tmpdir);
        extractionProgress = 100;
    }

    /** Распаковка tar-архива
     * @param in путь к распаковываемому архиву
     * @param out File-объект полученной директории
     * */
    private void decompressTar(String in, File out) throws IOException {
        try (TarArchiveInputStream fin = new TarArchiveInputStream(new FileInputStream(in))){
            TarArchiveEntry entry;
            while ((entry = fin.getNextTarEntry()) != null) {
                if (entry.isDirectory()) {
                    continue;
                }
                File curfile = new File(out, entry.getName());
                File parent = curfile.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
                IOUtils.copy(fin, new FileOutputStream(curfile));
            }
        }
    }

    /** Распаковка zip-архива
     * @param in путь к распакованному архиву
     * @param out File-объект полученной директории
     * */
    private void decompressZip(String in, File out) throws IOException {
        try (ZipArchiveInputStream fin = new ZipArchiveInputStream(new FileInputStream(in))){
            ZipArchiveEntry entry;
            while ((entry = fin.getNextZipEntry()) != null) {
                if (entry.isDirectory()) {
                    continue;
                }
                File curfile = new File(out, entry.getName());
                File parent = curfile.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
                IOUtils.copy(fin, new FileOutputStream(curfile));
            }
        }
    }

    /** Получение прогресса текущего разархивирования
     * @return прогресс текущего разархивирования
     * */
    public int getExtractionProgress() {
        return (int)Math.floor(extractionProgress);
    }




}
