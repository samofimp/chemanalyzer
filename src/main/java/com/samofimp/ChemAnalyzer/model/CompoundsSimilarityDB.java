package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/** Сходство патентов */
@Entity
@Table(name = "compounds_similarity")
public class CompoundsSimilarityDB {
    /** Идентификатор */
    @Id
    @GeneratedValue
    Long Id;

    /** Идентификатор первого патента */
    @Getter
    @Setter
    Long Compound1_Id;

    /** Идентификатор второго патента */
    @Getter
    @Setter
    Long Compound2_Id;

    /** Мера сходства двух патентов */
    @Getter
    @Setter
    Double Coefficient;

    /** Получение идентификатора схосдства патентов */
    public Long getId() {
        return Id;
    }

    /** Установка идентификатора сходства патентов */
    public void setId(Long Id) {
        this.Id = Id;
    }
}
