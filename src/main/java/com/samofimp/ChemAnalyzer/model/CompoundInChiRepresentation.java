package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.inchi.InChIGenerator;
import org.openscience.cdk.inchi.InChIGeneratorFactory;

/** InChi-представление химического соединения */
@NoArgsConstructor
public class CompoundInChiRepresentation {

    /** InChi-представление в строковом типе */
    @Getter
    @Setter
    String Formula;

    /** Получение InChi-представления из MOL-представления
     * @param MOL MOL-представление
     * */
    public void setFromMOL(CompoundMOLRepresentation MOL) throws CDKException {
        AtomContainer molecule = MOL.getMolecule();
        InChIGeneratorFactory factory = InChIGeneratorFactory.getInstance();
        InChIGenerator generator = factory.getInChIGenerator(molecule);
        this.Formula = generator.getInchi();
    }
}
