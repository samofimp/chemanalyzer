package com.samofimp.ChemAnalyzer.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/** Репозиторий сходств химических соединений */
@Repository
public class CompoundsSimilarityRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    /** Поиск всех записей из таблицы со сходствами химических соединений
     * @return список сходств химических соединений
     * */
    public List<CompoundsSimilarityDB> findAll() {
        String sql = "select * from compounds_similarity";
        List<CompoundsSimilarityDB> compoundsSimilarities = new ArrayList<CompoundsSimilarityDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            CompoundsSimilarityDB compoundsSimilarity = new CompoundsSimilarityDB();
            compoundsSimilarity.setId((Long)row.get("id"));
            compoundsSimilarity.setCompound1_Id((Long)row.get("compound1_id"));
            compoundsSimilarity.setCompound2_Id((Long)row.get("compound2_id"));
            compoundsSimilarity.setCoefficient((Double)row.get("coeff"));
            compoundsSimilarities.add(compoundsSimilarity);
        }
        return compoundsSimilarities;
    }

    /** Поиск записей из таблицы со сходствами химических соединений в заданном интервале по идентификаторам
     * @param id1 левая граница интервала
     * @param id2 правая граница интервала
     * @return список сходств химических соединений
     * */
    public List<CompoundsSimilarityDB> findFromTo(Long id1, Long id2) {
        String sql = "select * from compounds_similarity where id between" + id1.toString() + " and " + id2.toString();
        List<CompoundsSimilarityDB> compoundsSimilarities = new ArrayList<CompoundsSimilarityDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            CompoundsSimilarityDB compoundsSimilarity = new CompoundsSimilarityDB();
            compoundsSimilarity.setId((Long)row.get("id"));
            compoundsSimilarity.setCompound1_Id((Long)row.get("compound1_id"));
            compoundsSimilarity.setCompound2_Id((Long)row.get("compound2_id"));
            compoundsSimilarity.setCoefficient((Double)row.get("coeff"));
            compoundsSimilarities.add(compoundsSimilarity);
        }
        return compoundsSimilarities;
    }

    /** Нахождение сходства химических соединений по идентификатору
     * @param id идентификатор
     * @return запись химического сходства*/
    public CompoundsSimilarityDB findById(Long id) {
        CompoundsSimilarityDB compoundsSimilarity = new CompoundsSimilarityDB();
        compoundsSimilarity.setId(jdbcTemplate.queryForObject("select id from compounds_similarity where id = " + id.toString(), Long.class));
        compoundsSimilarity.setCompound1_Id(jdbcTemplate.queryForObject("select compound1_id from compounds_similarity where id = " + id.toString(), Long.class));
        compoundsSimilarity.setCompound2_Id(jdbcTemplate.queryForObject("select compound2_id from compounds_similarity where id = " + id.toString(), Long.class));
        compoundsSimilarity.setCoefficient(jdbcTemplate.queryForObject("select coeff from compounds_similarity where id = " + id.toString(), Double.class));
        return compoundsSimilarity;
    }

    /** Нахождение общего числа записей в таблице сходства химических соединений
     * @return общее число записей в таблице
     * */
    public int findTotalCount() {
        String sql = "select count(*) from compounds_similarity";
        int total = jdbcTemplate.queryForObject(sql, int.class);
        return total;
    }

    /** Удаление записи сходства химических соединений по идентификатору
     * @param id идентификатор удаляемой записи
     * */
    public void deleteById(Long id) {
        jdbcTemplate.update("delete from compounds_similarity where id = " + id.toString());
    }

    /** Вставка в таблицу записи сходства химических соединений
     * @param compoundsSimilarity химическое сходство
     * */
    public void insert(CompoundsSimilarityDB compoundsSimilarity) {
        String sql = "insert into compounds_similarity (compound1_id, compound2_id, coeff) " +
                "values(?, ?, ?)";
        Object[] params = {compoundsSimilarity.getCompound1_Id(), compoundsSimilarity.getCompound2_Id(), compoundsSimilarity.getCoefficient()};
        int[] types = {Types.BIGINT, Types.BIGINT, Types.DOUBLE};
        jdbcTemplate.update(sql, params, types);
    }

    /** Получение из таблицы сходств химических соединений последнего идентификатора
     * @return последний идентификатор
     * */
    public Long getLastId() {
        return jdbcTemplate.queryForObject("select top 1 id from compounds_similarity order by id desc", Long.class);
    }
}
