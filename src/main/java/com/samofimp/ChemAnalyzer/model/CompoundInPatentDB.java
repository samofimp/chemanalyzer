package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/** Соединение в архиве */
@Entity
@Table(name = "compound_in_patent")
public class CompoundInPatentDB {

    /** Идентификатор соединения в архиве */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long Id;

    /** Идентификатор соединения */
    @Getter
    @Setter
    Long Compound_Id;

    /** Идентификатор патента */
    @Getter
    @Setter
    Long Patent_Id;

    /** Получение идентификатора соединения в архиве */
    public Long getId() {
        return Id;
    }

    /** Установка идентификатора соединения в архиве */
    public void setId(Long Id) {
        this.Id = Id;
    }
}
