package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

/** Патент */
@NoArgsConstructor
public class Patent {

    /** Идентификатор */
    @Getter
    @Setter
    Long Id;

    /** Название */
    @Getter
    @Setter
    String Name;

    /** Номер публикации */
    @Getter
    @Setter
    String PublicationNumber;

    /** Дата публикации */
    @Getter
    @Setter
    LocalDate PublicationDate;

    /** Заявитель */
    @Getter
    @Setter
    String Applicant;

    /** Классификация по МПК */
    @Getter
    @Setter
    String IPCClassification;

    /** Список соединений в патенте */
    public List<Compound> compounds;

    /** Приведение к типам базы данных
     * @return патент в типах базы данных
     * */
    public PatentDB ToDatabase() {
        PatentDB patentDB = new PatentDB();
        patentDB.setId(Id);
        patentDB.setName(Name);
        patentDB.setPublicationNumber(PublicationNumber);
        patentDB.setPublicationDate(Date.valueOf(PublicationDate));
        patentDB.setApplicant(Applicant);
        patentDB.setIPCClassification(IPCClassification);
        return patentDB;
    }
}
