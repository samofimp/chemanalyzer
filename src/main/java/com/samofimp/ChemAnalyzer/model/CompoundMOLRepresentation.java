package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.Setter;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.io.MDLV2000Reader;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/** MOL-представление химического соединения */
public class CompoundMOLRepresentation {

    /** AtomContainer-объект химического соединения */
    @Getter
    @Setter
    AtomContainer molecule;

    /** FileReader для файла с химическим соединением */
    @Getter
    @Setter
    FileReader MOLFileReader;

    /** StringReader для строки с химическим соединением */
    @Getter
    @Setter
    StringReader MOLStringReader;

    /** MOL-представление в строке */
    @Getter
    @Setter
    String MOLInString;

    /** Получение MOL-представления из StringReader
     * @param reader StringReader для строки с химическим соединением
     * */
    public void SetMoleculeFromReader(StringReader reader) throws CDKException, IOException {
        MOLStringReader = reader;
        MDLV2000Reader molreader = new MDLV2000Reader(reader);
        AtomContainer newMolecule = new AtomContainer();
        molreader.read(newMolecule);
        this.molecule = newMolecule;
    }

    /** Получение MOL-представления из файла
     * @param directory путь к MOL-файлу
     * */
    public void setMoleculeFromPath(String directory) throws IOException, CDKException {
        File MOLFile = new File(directory);
        MOLFileReader = new FileReader(MOLFile);
        FileReader reader = MOLFileReader;
        byte[] molBytes = Files.readAllBytes(Paths.get(directory));
        MOLInString = new String(molBytes);
        MDLV2000Reader molreader = new MDLV2000Reader(reader);
        AtomContainer newMolecule = new AtomContainer();
        molreader.read(newMolecule);
        this.molecule = newMolecule;
    }
}
