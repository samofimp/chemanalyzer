package com.samofimp.ChemAnalyzer.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/** Репозиторий соединений в патентах */
@Repository
public class CompoundsInPatentsRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    /** Нахождение всех записей с соединениями в патентах из таблицы
     * @return список соединений в патентах
     * */
    public List<CompoundInPatentDB> findAll() {
        String sql = "select * from compound_in_patent";
        List<CompoundInPatentDB> compoundsInPatents = new ArrayList<CompoundInPatentDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            CompoundInPatentDB compoundInPatent = new CompoundInPatentDB();
            compoundInPatent.setId((Long)row.get("id"));
            compoundInPatent.setCompound_Id((Long)row.get("compound_id"));
            compoundInPatent.setPatent_Id((Long)row.get("patent_id"));
            compoundsInPatents.add(compoundInPatent);
        }
        return compoundsInPatents;
    }

    /** Нахождение записей с соединениями в патентах из таблицы в указанном интервале идентификаторов
     * @param id1 левая граница идентификаторов
     * @param id2 правая граница идентификаторов
     * @return список соединений в патентах
     * */
    public List<CompoundInPatentDB> findFromTo(Long id1, Long id2) {
        String sql = "select * from patent where id between" + id1.toString() + " and " + id2.toString();
        List<CompoundInPatentDB> compoundsInPatents = new ArrayList<CompoundInPatentDB>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            CompoundInPatentDB compoundInPatent = new CompoundInPatentDB();
            compoundInPatent.setId((Long)row.get("id"));
            compoundInPatent.setCompound_Id((Long)row.get("compound_id"));
            compoundInPatent.setPatent_Id((Long)row.get("patent_id"));
            compoundsInPatents.add(compoundInPatent);
        }
        return compoundsInPatents;
    }

    /** Нахождение записи с соединением в патенте из таблицы по указанному идентификатору
     * @param id идентификатор искомой записи
     * @return запись с соединением в патенте
     * */
    public CompoundInPatentDB findById(Long id) {
        CompoundInPatentDB compoundInPatent = new CompoundInPatentDB();
        compoundInPatent.setId(jdbcTemplate.queryForObject("select id from compound_in_patent where id = " + id.toString(), Long.class));
        compoundInPatent.setCompound_Id(jdbcTemplate.queryForObject("select compound_id from compound_in_patent where id = " + id.toString(), Long.class));
        compoundInPatent.setPatent_Id(jdbcTemplate.queryForObject("select patent_id from compound_in_patent where id = " + id.toString(), Long.class));
        return compoundInPatent;
    }

    /** Получение числа записей с соединениями в патентах в таблице
     * @return число записей с соединениями в патентах в таблице
     * */
    public int findTotalCount() {
        String sql = "select count(*) from compound_in_patent";
        int total = jdbcTemplate.queryForObject(sql, int.class);
        return total;
    }

    /** Удаление записи с соединением в патенте из таблицы по идентификатору
     * @param id идентификатор удаляемой записи
     * */
    public void deleteById(Long id) {
        jdbcTemplate.update("delete from compound_in_patent where id = " + id.toString());
    }

    /** Вставка записи с соединением в патенте в таблицу
     * @param compoundInPatent соединение в патенте
     * */
    public void insert(CompoundInPatentDB compoundInPatent) {
        String sql = "insert into compound_in_patent (compound_id, patent_id) " +
                "values(?, ?)";
        Object[] params = {compoundInPatent.getCompound_Id(), compoundInPatent.getPatent_Id()};
        int[] types = {Types.BIGINT, Types.BIGINT};
        jdbcTemplate.update(sql, params, types);
    }

    /** Получение последнего идентификатора из таблицы с соединениями в патентах
     * @return последний идентификатор
     * */
    public Long getLastId() {
        return jdbcTemplate.queryForObject("select top 1 id from compound_in_patent order by id desc", Long.class);
    }
}
