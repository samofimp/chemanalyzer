package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.fingerprint.Fingerprinter;
import org.openscience.cdk.fingerprint.IBitFingerprint;

import java.util.BitSet;

/** Отпечаток молекулы */
@NoArgsConstructor
public class CompoundFingerprintRepresentation {

    /** Отпечаток молекулы в наборе бит */
    @Getter
    @Setter
    BitSet Fingerprint;

    /** Получение отпечатка молекулы из MOL-представления
     * @param MOL MOL-представление
     * */
    public void SetFromMOL(CompoundMOLRepresentation MOL) throws CDKException {
        AtomContainer molecule = MOL.getMolecule();
        Fingerprinter fingerprinter = new Fingerprinter();
        IBitFingerprint fingerprint = fingerprinter.getBitFingerprint(molecule);
        this.Fingerprint = fingerprint.asBitSet();
    }
}
