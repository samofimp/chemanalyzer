package com.samofimp.ChemAnalyzer.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;

/** SMILES-представление химического соединения */
@NoArgsConstructor
public class CompoundSMILESRepresentation {

    /** SMILES-формула в строке */
    @Getter
    @Setter
    String Formula;

    /** Получение SMILES-представления из MOL-представления
     * @param MOL MOL-представление
     * */
    public void setFromMOL(CompoundMOLRepresentation MOL) throws CDKException {
        AtomContainer molecule = MOL.getMolecule();
        SmilesGenerator generator = new SmilesGenerator(SmiFlavor.Generic);
        this.Formula = generator.create(molecule);
    }
}
